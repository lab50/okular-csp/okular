#!/bin/sh
# A wrap around find-scriptlet-requires for Alt.

FINDREQ=/usr/lib/rpm/find-scriptlet-requires
if [ -x "${FINDREQ}" ]
then
    "${FINDREQ}" $* | sed -e '/cpconfig/d'
fi
