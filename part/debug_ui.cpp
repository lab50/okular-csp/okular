/*
    SPDX-FileCopyrightText: 2014 Frederik Gladhorn <gladhorn@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "debug_ui.h"

Q_LOGGING_CATEGORY(OkularUiDebug, "net.lab50.okularcsp.ui", QtWarningMsg)
