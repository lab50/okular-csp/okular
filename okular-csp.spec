%define _unpackaged_files_terminate_build 0
%undefine _annotated_build
%global is_redos %( if [ -f /etc/redos-release ]; then echo "1"; else echo "0"; fi )
%global is_rosa %( if [ -f /etc/rosa-release ]; then echo "1"; else echo "0"; fi )
%global is_alt %( if [ -f /etc/altlinux-release ]; then echo "1"; else echo "0"; fi )
%global old_name okular-csp

# Пакеты по умолчанию.
%global cmake_pkg cmake extra-cmake-modules cmake-rpm-macros
%global chm_pkg chmlib-devel
%global kde_macros_pkg kf5-rpm-macros
%global khelpcenter_pkg khelpcenter
# Пути по умолчанию.
%define kcfg_dir %{_kf5_datadir}/config.kcfg
%define kde_applications_dir %{_kf5_datadir}/applications
%define _kf5_metainfodir %{_kf5_datadir}/metainfo

# У Fedora есть макросы _kf5_bindir, _kf5_datadir и т.д. Сделаем, чтобы они были доступны везде.
# TODO: использовать elif, когда он станет доступен.
%if %is_rosa
%global cmake_pkg cmake extra-cmake-modules
%global kde_macros_pkg kde5-macros
%global chm_pkg lib64chm-devel
%global khelpcenter_pkg plasma5-khelpcenter
%global _kf5_bindir %{_kde5_bindir}
%global _kf5_datadir %{_kde5_datadir}
%global _kf5_docdir %{_kde5_docdir}
%define kde_applications_dir %{_kde5_datadir}/applications/kde5
# У Росы нет %cmake_kf5, %cmake_build и %cmake_install.
%define cmake_kf5 %cmake_kde5
%define cmake_build %make
%define cmake_install %makeinstall_std -C build
%endif

%if %is_alt
# Note: без no_altplace используются какие-то странные пути вроде /usr/lib/kf5/bin.
%K5init no_altplace man
%global cmake_pkg cmake extra-cmake-modules
%global kde_macros_pkg rpm-build-kf5
%global chm_pkg libchm-devel
%global khelpcenter_pkg kde5-khelpcenter
%global _kf5_bindir %{_K5bin}
%global _kf5_datadir %{_datadir}
%global _kf5_docdir %{_datadir}/doc
%define kde_applications_dir %{_K5xdgapp}
%global kcfg_dir %{_datadir}/config.kcfg
# Чтобы избавиться от cpconfig в Requires(post).
%define __find_scriptlet_requires %{_builddir}/%{name}-%{version}/find-scriptlet-requires.sh
%endif

Name: okular-gost
Version: 21.11.70.35
Release: 1%{?dist}
%if %is_alt || %is_rosa
Group: Office
%endif
License: GPLv2
Summary: Окуляр ГОСТ
URL: https://gitlab.com/lab50/okular-csp/okular
Source0: %{name}-%{version}.tar.gz

BuildRequires: gcc-c++
BuildRequires: %{cmake_pkg}
BuildRequires: gdb
BuildRequires: desktop-file-utils
# KDE-макросы для сборки пакетов.
BuildRequires: %{kde_macros_pkg}

# Зависимости Qt.
BuildRequires: pkgconfig(Qt5DBus)
BuildRequires: pkgconfig(Qt5PrintSupport)
BuildRequires: pkgconfig(Qt5Qml)
BuildRequires: pkgconfig(Qt5Quick)
BuildRequires: pkgconfig(Qt5Svg)
BuildRequires: pkgconfig(Qt5Test)
BuildRequires: pkgconfig(Qt5TextToSpeech)
BuildRequires: pkgconfig(Qt5Widgets)

# Зависимости KDE.
%if %is_alt
BuildRequires: kde5-libkexiv2-devel
BuildRequires: kf5-karchive-devel
BuildRequires: kf5-kbookmarks-devel
BuildRequires: kf5-kcompletion-devel
BuildRequires: kf5-kconfig-devel
BuildRequires: kf5-kconfigwidgets-devel
BuildRequires: kf5-kcoreaddons-devel
BuildRequires: kf5-kdbusaddons-devel
BuildRequires: kf5-kdoctools-devel
BuildRequires: kf5-kiconthemes-devel
BuildRequires: kf5-kio-devel
BuildRequires: kf5-kjs-devel
BuildRequires: kf5-khtml-devel
BuildRequires: kf5-kparts-devel
BuildRequires: kf5-kpty-devel
BuildRequires: kf5-kwindowsystem-devel
BuildRequires: kf5-threadweaver-devel
# Note: без kf5-kio не работает "сохранить как".
Requires: kf5-kio
# Note: без icon-theme-breeze нет некоторых иконок в GUI.
Requires: icon-theme-breeze
%else
BuildRequires: cmake(KF5Archive)
BuildRequires: cmake(KF5Bookmarks)
BuildRequires: cmake(KF5Completion)
BuildRequires: cmake(KF5Config)
BuildRequires: cmake(KF5ConfigWidgets)
BuildRequires: cmake(KF5CoreAddons)
BuildRequires: cmake(KF5DBusAddons)
BuildRequires: cmake(KF5DocTools)
BuildRequires: cmake(KF5IconThemes)
BuildRequires: cmake(KF5JS)
BuildRequires: cmake(KF5KIO)
BuildRequires: cmake(KF5Parts)
BuildRequires: cmake(KF5Pty)
BuildRequires: cmake(KF5ThreadWeaver)
BuildRequires: cmake(KF5KHtml)
BuildRequires: cmake(KF5WindowSystem)
BuildRequires: cmake(KF5KExiv2)
%endif
BuildRequires: pkgconfig(phonon4qt5)
# Note: без khelpcenter не открывается справка.
Requires: %{khelpcenter_pkg}

BuildRequires: pkgconfig(libzip)
BuildRequires: pkgconfig(librsvg-2.0)
BuildRequires: ebook-tools-devel
BuildRequires: pkgconfig(libjpeg)
BuildRequires: pkgconfig(libtiff-4)
BuildRequires: pkgconfig(freetype2)
BuildRequires: pkgconfig(libspectre)
BuildRequires: pkgconfig(poppler-csp-qt5) >= 21.12.0.50
BuildRequires: pkgconfig(%{old_name}-common) >= %{version}
BuildRequires: %{chm_pkg}
BuildRequires: pkgconfig(zlib)
BuildRequires: pkgconfig(ddjvuapi)
BuildRequires: pkgconfig(libmarkdown)
%if %is_rosa || %is_redos
BuildRequires: cmake(QMobipocket)
%endif

Requires: %{old_name}-common%{?_isa} >= %{version}-%{release}
# NOTE: It seems there is no Suggests/Recommends on Alt.
%if ! %is_alt
Suggests: %{old_name}-extra-backends%{?_isa} = %{version}-%{release}
%endif
Requires(post): /usr/bin/ldd
Requires(post): /sbin/ldconfig

%description
 «Окуляр ГОСТ» — это универсальная программа для просмотра документов,
 поддерживающая просмотр, заполнение форм, комментирование,
 а также работу с встроенными файлами.

 Модули дополнительных форматов находятся в пакете
 okular-csp-extra-backends.

%package -n lib%{old_name}-core
%if %is_alt || %is_rosa
Group: Office
%endif
Summary: Окуляр ГОСТ: библиотека для модулей форматов
Obsoletes: %{old_name}-libs <= 21.11.70.31

%description -n lib%{old_name}-core
 Библиотека предоставляет общие функции для модулей форматов
 «Окуляр ГОСТ».

%package -n %{old_name}-extra-backends
%if %is_alt || %is_rosa
Group: Office
%endif
Summary: Окуляр ГОСТ: модули дополнительных форматов
Requires: lib%{old_name}-core%{?_isa} = %{version}-%{release}
%if %is_alt
# Requires: ghostscript-classic
# Requires: texlive-base-bin
%else
Suggests: texlive-dvipdfmx
Recommends: ghostscript
%endif
# translations moved here
Conflicts: kde-l10n < 17.03
Obsoletes: %{old_name}-part <= 21.11.70.31

%description -n %{old_name}-extra-backends
 Модули позволяют открывать в «Окуляр ГОСТ» следующие форматы:
  * DeJaVu
  * TIFF, включая многостраничные
  * EPUB
  * Markdown
  * DVI
  * XPS
  * ComicBook, FictionBook и Plucker

 «Окуляр ГОСТ» — это универсальная программа для просмотра документов.

%package -n %{old_name}
%if %is_alt || %is_rosa
Group: Office
%endif
Summary: Окуляр ГОСТ с модулем КриптоПро (метапакет)
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: lib%{old_name}-cryptopro%{?_isa} = %{version}-%{release}
Requires: cprocsp-rdr-gui-gtk-64 >= 5.0

%description -n %{old_name}
 Пакет устанавливает все требуемые пакеты для работы с электронной
 подписью средствами КриптоПро в «Окуляр ГОСТ».

 «Окуляр ГОСТ» — это универсальная программа для просмотра документов.

%prep
%setup

%build
%if %is_alt
%K5build -DBUILD_TESTING=OFF -DOKULAR_UI=desktop -DCMAKE_INSTALL_DOCDIR:PATH="share/doc/%{old_name}"
%else
%cmake_kf5 -DBUILD_TESTING=OFF -DOKULAR_UI=desktop -DCMAKE_INSTALL_DOCDIR:PATH="share/doc/%{old_name}"
%cmake_build
%endif

%install
%if %is_alt
%K5install
mkdir -p %{buildroot}/%{_qt5_plugindir}/kf5/kio
ln -s ../../okularcsp-chm.so %{buildroot}/%{_qt5_plugindir}/kf5/kio/okularcsp-chm.so
mv %{buildroot}/%{_libdir}/kf5/devel/libokularcsp-core.so %{buildroot}/%{_libdir}
# На Альте нет mobi?
rm -f %{buildroot}/%{_datadir}/locale/*/LC_MESSAGES/okularcsp_mobi.mo
%else
%cmake_install
%endif

%check
desktop-file-validate %{buildroot}%{kde_applications_dir}/net.lab50.okularcsp.desktop
# appstream-util validate-relax --nonet %{buildroot}%{_kf5_metainfodir}/net.lab50.okularcsp.appdata.xml

%files
# Note: у Alt нет /usr/share/licenses.
%if %is_alt
%doc LICENSES/*
%else
%license LICENSES/*
%endif
%doc %{_kf5_docdir}
%{_mandir}/man1/okularcsp.1.*
%{_mandir}/ru/man1/okularcsp.1.*
%{_kf5_bindir}/okularcsp
%{kde_applications_dir}/net.lab50.okularcsp.desktop
%{_kf5_metainfodir}/net.lab50.okularcsp.appdata.xml
%{_kf5_datadir}/okularcsp
%{_kf5_datadir}/kconf_update/okularcsp.upd
%{_kf5_datadir}/icons/hicolor/*/*/*
%{_kf5_datadir}/qlogging-categories5/okularcsp*
%{_kf5_datadir}/kxmlgui5/okularcsp/*.rc
%{kcfg_dir}/okularcsp.kcfg
%{kcfg_dir}/okularcsp_core.kcfg
%{_datadir}/locale/*/LC_MESSAGES/okularcsp.mo
# Part stuff.
%{_qt5_plugindir}/okularcsppart.so
%{_kf5_datadir}/kservices5/okularcsp_part.desktop
# Poppler stuff.
%{kde_applications_dir}/okularcspApplication_pdf.desktop
%{_kf5_metainfodir}/net.lab50.okularcsp-poppler.metainfo.xml
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_poppler.so
%{_kf5_datadir}/kservices5/okularcspPoppler.desktop
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_poppler.mo
%{kcfg_dir}/pdfsettings-csp.kcfg

%post
# For backward compatibility.
ln -f -s /usr/bin/okularcsp /usr/bin/okular-csp

# The libcurl problem.
if /usr/bin/ldd /usr/bin/okularcsp | grep --quiet 'libcurl.so'
then
    if [ -f /opt/cprocsp/sbin/amd64/cpconfig ]
    then
        current=`/opt/cprocsp/sbin/amd64/cpconfig -ini '\config\apppath\libcurl.so' -view`
        if [ "$current" = '/opt/cprocsp/lib/amd64/libcpcurl.so' ]
        then
            echo 'Oklar GOST have the libcurl dependency. Fixing...'
            new=`/sbin/ldconfig -p | grep libcurl.so.4 | tr ' ' '\n' | grep /`
            if [ ! -n "$new" ] && [ -f /lib64/libcurl.so.4 ]
            then
                new='/lib64/libcurl.so.4'
            fi
            # Update the /config/apppath/libcurl.so param.
            if [ -n "$new" ]
            then
                /opt/cprocsp/sbin/amd64/cpconfig -ini '\config\apppath' -add string libcurl.so "$new"
                echo "Done!"
            fi
        fi
    fi
fi

%postun
rm -f /usr/bin/okular-csp

%files -n lib%{old_name}-core
%{_libdir}/libokularcsp-core.so.9*
%exclude %{_libdir}/libokularcsp-core.so
%exclude %{_libdir}/cmake
%exclude %{_includedir}/okularcsp

%post -n lib%{old_name}-core -p /sbin/ldconfig
%postun -n lib%{old_name}-core -p /sbin/ldconfig

%files -n %{old_name}-extra-backends
%{kde_applications_dir}/okularcspApplication_chm.desktop
%{kde_applications_dir}/okularcspApplication_comicbook.desktop
%{kde_applications_dir}/okularcspApplication_djvu.desktop
%{kde_applications_dir}/okularcspApplication_dvi.desktop
%{kde_applications_dir}/okularcspApplication_epub.desktop
%{kde_applications_dir}/okularcspApplication_fax.desktop
%{kde_applications_dir}/okularcspApplication_fb.desktop
%{kde_applications_dir}/okularcspApplication_ghostview.desktop
%{kde_applications_dir}/okularcspApplication_kimgio.desktop
%{kde_applications_dir}/okularcspApplication_md.desktop
%{kde_applications_dir}/okularcspApplication_plucker.desktop
%{kde_applications_dir}/okularcspApplication_tiff.desktop
%{kde_applications_dir}/okularcspApplication_txt.desktop
%{kde_applications_dir}/okularcspApplication_xps.desktop
%{_kf5_metainfodir}/net.lab50.okularcsp-chm.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-comicbook.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-djvu.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-dvi.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-epub.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-fax.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-fb.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-kimgio.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-md.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-plucker.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-spectre.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-tiff.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-txt.metainfo.xml
%{_kf5_metainfodir}/net.lab50.okularcsp-xps.metainfo.xml
%{_kf5_datadir}/kservices5/okularcspChm.desktop
%{_kf5_datadir}/kservices5/okularcspComicbook.desktop
%{_kf5_datadir}/kservices5/okularcspDjvu.desktop
%{_kf5_datadir}/kservices5/okularcspDvi.desktop
%{_kf5_datadir}/kservices5/okularcspEPub.desktop
%{_kf5_datadir}/kservices5/okularcspFax.desktop
%{_kf5_datadir}/kservices5/okularcspFb.desktop
%{_kf5_datadir}/kservices5/okularcspGhostview.desktop
%{_kf5_datadir}/kservices5/okularcspKimgio.desktop
%{_kf5_datadir}/kservices5/okularcspMd.desktop
%{_kf5_datadir}/kservices5/okularcspPlucker.desktop
%{_kf5_datadir}/kservices5/okularcspTiff.desktop
%{_kf5_datadir}/kservices5/okularcspTxt.desktop
%{_kf5_datadir}/kservices5/okularcspXps.desktop
%{_kf5_datadir}/kservicetypes5/okularcsp-Generator.desktop
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_chmlib.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_comicbook.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_djvu.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_dvi.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_epub.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_fax.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_fb.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_ghostview.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_kimgio.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_md.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_plucker.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_tiff.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_txt.so
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_xps.so
%{_qt5_plugindir}/okularcsp-chm.so
%if %is_alt
%{_qt5_plugindir}/kf5/kio/okularcsp-chm.so
%endif
%{kcfg_dir}/gssettings-csp.kcfg
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_comicbook.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_chm.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_djvu.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_epub.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_dvi.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_fax.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_fictionbook.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_ghostview.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_kimgio.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_markdown.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_plucker.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_tiff.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_txt.mo
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_xps.mo

%if %is_rosa || %is_redos
%{kde_applications_dir}/okularcspApplication_mobi.desktop
%{_kf5_metainfodir}/net.lab50.okularcsp-mobipocket.metainfo.xml
%{_qt5_plugindir}/okularcsp/generators/okularGenerator_mobi.so
%{_kf5_datadir}/kservices5/okularcspMobi.desktop
%{_datadir}/locale/*/LC_MESSAGES/okularcsp_mobi.mo
%endif

%exclude %{kde_applications_dir}/net.lab50.mobile.okularcsp_*.desktop
%exclude %{_kf5_datadir}/kservices5/ms-its.protocol

%files -n %{old_name}

%changelog
 * Thu Jan 16 2025 Georgiy Sgibnev <georgiy@lab50.net> - 21.11.70.35
- Поддержка кириллических заметок в PDF.
- Использование собственного шрифта на Linux.
- Выражение {inn} - это только ИНН юр. лица (не ИП).
- Устранение ошибок при работе с ФС на Windows.
- Исправление проблемы с некорректным встраиванием некоторых SVG.
- Исправление ошибки, возникающей при получении имени пользователя на Росе.
- Отключение DBus на Windows

* Tue Nov 19 2024 Georgiy Sgibnev <georgiy@lab50.net> - 21.11.70.34
- Исправление ошибки при работе с самоподписанными сертификатами
- Поддержка доп. свойств сертификата: ИНН, СНИЛС, ОГРН
- Тюнинг обработки ошибок, возникающих при установке лицензии
- Теперь пакет okular-gost ставится на Альт даже при отсутствии КриптоПро
- Диалог лицензий и команда show-license выводят версию Окуляр ГОСТ для удобства
- Поддержка PDF с некорректным (пустым) объектом AcroForm

* Wed Oct  2 2024 Georgiy Sgibnev <georgiy@lab50.net> - 21.11.70.33
- Показ пользовательского соглашения
- Команды okularcsp-tool show-eula и okularcsp-tool install-license
- Если есть права суперпользователя, то лицензия записывается в /etc
- Примеры SVG-штампов переехали в пакет okular-csp-common
- Поддержка Astra Linux 1.8
- Форма создания ЭП запоминает выбор пользователя
- Больше информации при ошибке, связанной с неудачной установкой лицензии
- Больше автотестов
- Исправлен тестовый штамп stamp2.svg под Windows
- Исправлен файл copyright
- Правильное определение версии Windows 11

* Fri Jun 28 2024 Georgiy Sgibnev <georgiy@lab50.net> - 21.11.70.32
- На Linux можно устанавливать Окуляр ГОСТ без КриптоПро
- Меню 'Справка' для Windows теперь работает
- Решена проблема со встраиванием большого шрифта на Windows
- Вместо одного теперь два скрипта для автоматизации: sign-all.sh и sign-many.sh
- Можно хранить одну лицензию на всех пользователей: /etc/okular-csp/license.key
- Библиотека libOkularcsp5Core.so переименована в libokularcsp-core.so
- Библиотека libcryptopro_plugin.so переименована в libokularcsp-cryptopro.so
- Новая библиотека libokularcsp-common.so
- RPM-пакет okular-csp-part переименован в okular-csp-extra-backends
  (по аналогии с DEB-пакетом)
- RPM-пакет okular-csp теперь можно устанавливать без okular-csp-extra-backends
- Пакет okular-csp-cryptopro переименован в libokular-csp-cryptopro
- DEB-пакет libokular-csp5core9 переименован в libokular-csp-core
- RPM-пакет okular-csp-libs переименован в libokular-csp-core
- Новый метапакет okular-csp-cryptopro-gui
- Поддержка Ubuntu 24.04 LTS
- Поддержка Альт 11
- Исправление регистров символов в ФИО

* Mon Jun 03 2024 Georgiy Sgibnev <georgiy@lab50.net> - 21.11.70.29-0.1
- Версия для Windows 10
- Поддерживается сертифицированная версия КриптоПро CSP 5.0.12000 и более поздние версии,
  более ранние — не поддерживаются
- Графическая форма для установки лицензии
- Улучшена поддержка SVG с градиентами
- Исправлена ошибка в форме настройки модулей при отсутствии сертификатов
- Исправлена ошибка, связанная с перезаписью файла на сетевой ФС
- Не открывается второе окно, если подписанный PDF перезаписывает исходный PDF
- pdfcpro show-license теперь выводит краткую информацию по окружению
- Новый штамп по умолчанию в платной версии
- Исправлена ошибка с иконкой программы на Fedora и РЕД ОС
- Название исполняемого файла теперь okularcsp, есть символьная ссылка okular-csp
  для обеспечения обратной совместимости
- Мелкие правки перевода для интерфейса
- Более подробные сообщения об ошибках
- Более подробное описание проблем с валидацией цепочки сертификатов
- Решение проблемы с libcurl на РЕД ОС 8 и Fedaora 40
- Решение проблемы с man под Alt
- Новое выражение {subject/fullname} для шаблонов

* Wed Dec 15 2021 Nikolay Fedoruk <nfedoruk@lab50.net> - 21.04.4.3
- 21.04.4.2

* Tue Nov 02 2021 Rex Dieter <rdieter@fedoraproject.org> - 21.08.3-1
- 21.08.3

* Wed Oct 20 2021 Rex Dieter <rdieter@fedoraproject.org> - 21.08.2-1
- 21.08.2

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 21.04.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Thu Jun 10 2021 Rex Dieter <rdieter@fedoraproject.org> - 21.04.2-1
- 21.04.2

* Tue May 11 2021 Rex Dieter <rdieter@fedoraproject.org> - 21.04.1-1
- 21.04.1

* Sun Apr 18 2021 Rex Dieter <rdieter@fedoraproject.org> - 21.04.0-1
- 21.04.0
%makeinstall_std -C build
* Wed Mar 03 2021 Rex Dieter <rdieter@fedoraproject.org> - 20.12.3-1
- 20.12.3

* Wed Feb 03 2021 Rex Dieter <rdieter@fedoraproject.org> - 20.12.2-2
- Requires: kf5-kirigami2 (runtime dep, not linked)

* Wed Feb 03 2021 Rex Dieter <rdieter@fedoraproject.org> - 20.12.2-1
- 20.12.2

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 20.08.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Tue Nov 10 2020 Rex Dieter <rdieter@fedoraproject.org> - 20.08.3-2
- backport back/forward navigation fix (#1896246)

* Fri Nov  6 2020 Rex Dieter <rdieter@fedoraproject.org> - 20.08.3-1
- 20.08.3

* Tue Sep 15 2020 Rex Dieter <rdieter@fedoraproject.org> - 20.08.1-1
- 20.08.1

* Tue Aug 18 2020 Rex Dieter <rdieter@fedoraproject.org> - 20.08.0-1
- 20.08.0

* Mon Aug 10 2020 Rex Dieter <rdieter@fedoraproject.org> - 20.04.3-4
- use new cmake macros

* Sat Aug 01 2020 Fedora Release Engineering <releng@fedoraproject.org> - 20.04.3-3
- Second attempt - Rebuilt for
  https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 20.04.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Fri Jul 10 2020 Rex Dieter <rdieter@fedoraproject.org> - 20.04.3-1
- 20.04.3

* Fri Jun 12 2020 Rex Dieter <rdieter@fedoraproject.org> - 20.04.2-1
- 20.04.2

* Thu Jun 11 2020 Marie Loise Nolden <loise@kde.org> - 20.04.1-1	
- 20.04.1

* Sun Mar 22 2020 Rex Dieter <rdieter@fedoraproject.org> - 19.12.3-2
- Security fix for CVE-2020-9359 (#1815651,1815652)

* Fri Mar 06 2020 Rex Dieter <rdieter@fedoraproject.org> - 19.12.3-1
- 19.12.3

* Tue Feb 04 2020 Rex Dieter <rdieter@fedoraproject.org> - 19.12.2-1
- 19.12.2

* Thu Jan 30 2020 Rex Dieter <rdieter@fedoraproject.org> - 19.12.1-1
- 19.12.1

* Wed Jan 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 19.08.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Fri Jan 17 2020 Marek Kasik <mkasik@redhat.com> - 19.08.3-2
- Rebuild for poppler-0.84.0

* Tue Nov 12 2019 Rex Dieter <rdieter@fedoraproject.org> - 19.08.3-1
- 19.08.3

* Thu Oct 17 2019 Rex Dieter <rdieter@fedoraproject.org> - 19.08.2-1
- 19.08.2

* Mon Sep 30 2019 Rex Dieter <rdieter@fedoraproject.org> - 19.08.1-1
- 19.08.1

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 19.04.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Fri Jul 12 2019 Rex Dieter <rdieter@fedoraproject.org> - 19.04.3-1
- 19.04.3

* Tue Jun 04 2019 Rex Dieter <rdieter@fedoraproject.org> - 19.04.2-1
- 19.04.2

* Fri Mar 08 2019 Rex Dieter <rdieter@fedoraproject.org> - 18.12.3-1
- 18.12.3

* Tue Feb 05 2019 Rex Dieter <rdieter@fedoraproject.org> - 18.12.2-1
- 18.12.2

* Fri Feb 01 2019 Fedora Release Engineering <releng@fedoraproject.org> - 18.12.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Tue Jan 08 2019 Rex Dieter <rdieter@fedoraproject.org> - 18.12.1-1
- 18.12.1

* Sun Dec 16 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.12.0-1
- 18.12.0

* Tue Nov 06 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.08.3-1
- 18.08.3

* Wed Oct 10 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.08.2-1
- 18.08.2

* Fri Sep 07 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.08.1-1
- 18.08.1
- -DBUILD_OKULARKIRIGAMI:BOOL=OFF for now

* Fri Aug 10 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.04.3-1
- 18.04.3

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 18.03.90-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Sun Apr 08 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.03.90-1
- 18.03.90, support kcrash
- enable discount/libmarkdown support

* Fri Mar 23 2018 Marek Kasik <mkasik@redhat.com> - 17.12.3-2
- Rebuild for poppler-0.63.0

* Tue Mar 06 2018 Rex Dieter <rdieter@fedoraproject.org> - 17.12.3-1
- 17.12.3

* Thu Feb 08 2018 Rex Dieter <rdieter@fedoraproject.org> - 17.12.2-1
- 17.12.2

* Thu Feb 08 2018 Fedora Release Engineering <releng@fedoraproject.org> - 17.12.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jan 18 2018 Igor Gnatenko <ignatenkobrain@fedoraproject.org> - 17.12.1-2
- Remove obsolete scriptlets

* Thu Jan 11 2018 Rex Dieter <rdieter@fedoraproject.org> - 17.12.1-1
- 17.12.1

* Thu Dec 28 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.12.0-1
- 17.12.0

* Tue Nov 21 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.08.3-3
- BR: Qt5TextToSpeech, support %%bootstrap

* Wed Nov 08 2017 David Tardon <dtardon@redhat.com> - 17.08.3-2
- rebuild for poppler 0.61.0

* Wed Nov 08 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.08.3-1
- 17.08.3

* Mon Nov 06 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.08.2-2
- rebuild (ecm)

* Wed Oct 11 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.08.2-1
- 17.08.2

* Fri Oct 06 2017 David Tardon <dtardon@redhat.com> - 17.08.1-2
- rebuild for poppler 0.60.1

* Thu Sep 28 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.08.1-1
- 17.08.1

* Fri Sep 08 2017 David Tardon <dtardon@redhat.com> - 17.04.3-2
- rebuild for poppler 0.59.0

* Thu Aug 03 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.04.3-1
- 17.04.3

* Thu Aug 03 2017 David Tardon <dtardon@redhat.com> - 17.04.2-4
- rebuild for poppler 0.57.0

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 17.04.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 17.04.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Thu Jun 15 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.04.2-1
- 17.04.2

* Thu May 11 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.04.1-1
- 17.04.1

* Fri May 05 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.04.0-1
- 17.04.0

* Tue Mar 28 2017 David Tardon <dtardon@redhat.com> - 16.12.3-2
- rebuild for poppler 0.53.0

* Thu Mar 09 2017 Rex Dieter <rdieter@fedoraproject.org> - 16.12.3-1
- 16.12.3

* Thu Feb 09 2017 Rex Dieter <rdieter@fedoraproject.org> - 16.12.2-1
- 16.12.2

* Thu Jan 12 2017 Rex Dieter <rdieter@fedoraproject.org> - 16.12.1-1
- 16.12.1, kf5-ize

* Wed Nov 30 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.08.3-1
- 16.08.3, disable tests
- -devel: Provides: okular4-devel
- -part: Provides: okular4-part

* Thu Oct 13 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.08.2-1
- 16.08.2

* Wed Sep 07 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.08.1-1
- 16.08.1

* Sat Aug 13 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.08.0-1
- 16.08.0

* Sat Aug 06 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.07.90-1
- 16.07.90

* Sat Jul 30 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.07.80-1
- 16.07.80

* Sun Jul 10 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.04.3-1
- 16.04.3

* Sun Jun 12 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.04.2-1
- 16.04.2

* Sun May 08 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.04.1-1
- 16.04.1

* Wed Apr 20 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.04.0-2
- rebuild (qt)

* Mon Apr 18 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.04.0-1
- 16.04.0

* Thu Apr 14 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.03.80-1
- 16.03.80

* Tue Mar 15 2016 Rex Dieter <rdieter@fedoraproject.org> - 15.12.3-1
- 15.12.3, retire -active on f24+

* Mon Feb 15 2016 Rex Dieter <rdieter@fedoraproject.org> - 15.12.2-1
- 15.12.2

* Fri Feb 05 2016 Rex Dieter <rdieter@fedoraproject.org> 15.12.1-3
- cleanup, use %%license, -libs: Recommends: cups-client ghostscript-core

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 15.12.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Sat Jan 30 2016 Rex Dieter <rdieter@fedoraproject.org> - 15.12.1-1
- 15.12.1

* Sat Dec 05 2015 Rex Dieter <rdieter@fedoraproject.org> - 15.08.3-1
- 15.08.3

* Sat Oct 03 2015 Rex Dieter <rdieter@fedoraproject.org> - 15.08.1-1
- 15.08.1

* Thu Aug 20 2015 Than Ngo <than@redhat.com> - 15.08.0-1
- 15.08.0

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 15.04.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Jun 10 2015 Rex Dieter <rdieter@fedoraproject.org> - 15.04.2-1
- 15.04.2

* Tue May 26 2015 Rex Dieter <rdieter@fedoraproject.org> - 15.04.1-1
- 15.04.1

* Thu Apr 23 2015 Rex Dieter <rdieter@fedoraproject.org> 15.04.0-2
- make (kde4) lib build dependencies libkipi, qmobipocket unversioned

* Tue Apr 14 2015 Rex Dieter <rdieter@fedoraproject.org> - 15.04.0-1
- 15.04.0

* Sun Mar 01 2015 Rex Dieter <rdieter@fedoraproject.org> - 14.12.3-1
- 14.12.3

* Tue Feb 24 2015 Than Ngo <than@redhat.com> - 14.12.2-1
- 14.12.2

* Sat Jan 17 2015 Rex Dieter <rdieter@fedoraproject.org> - 14.12.1-1
- 14.12.1

* Fri Jan 16 2015 Rex Dieter <rdieter@fedoraproject.org> 14.11.97-3
- -libs: Requires: fix typo (#1183110), but omit (for now)

* Tue Dec 30 2014 Rex Dieter <rdieter@fedoraproject.org> 14.11.97-2
- -libs: Requires: cups(lpr) ps2pdf pdf2ps

* Mon Dec 08 2014 Rex Dieter <rdieter@fedoraproject.org> - 14.11.97-1
- 14.11.97

* Sat Nov 08 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.14.3-1
- 4.14.3

* Fri Oct 31 2014 Rex Dieter <rdieter@fedoraproject.org> 4.14.2-2
- BR: pkgconfig(kscreen)

* Sat Oct 11 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.14.2-1
- 4.14.2

* Mon Sep 15 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.14.1-1
- 4.14.1

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.14.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Thu Aug 14 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.14.0-1
- 4.14.0

* Tue Aug 05 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.97-1
- 4.13.97

* Mon Jul 14 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.3-1
- 4.13.3

* Mon Jun 09 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.2-1
- 4.13.2

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.13.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu May 29 2014 Kevin Kofler <Kevin@tigcc.ticalc.org> - 4.13.1-3
- restore -active support (the plasma-mobile FTBFS is long fixed)

* Mon May 12 2014 Rex Dieter <rdieter@fedoraproject.org> 4.13.1-2
- drop -active support (until plasma-mobile FTBFS is fixed)

* Sun May 11 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.1-1
- 4.13.1

* Sat Apr 12 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.13.0-1
- 4.13.0

* Fri Apr 04 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.97-1
- 4.12.97

* Sat Mar 22 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.95-1
- 4.12.95

* Wed Mar 19 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.90-1
- 4.12.90

* Fri Mar 07 2014 Rex Dieter <rdieter@fedoraproject.org> 4.12.3-2
- drop BR: libkipi-devel (only needs libkexiv2)

* Sat Mar 01 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.3-1
- 4.12.3

* Fri Jan 31 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.2-1
- 4.12.2

* Fri Jan 10 2014 Rex Dieter <rdieter@fedoraproject.org> - 4.12.1-1
- 4.12.1

* Fri Jan 03 2014 Rex Dieter <rdieter@fedoraproject.org> 4.12.0-4
- %%check: verify kimgio/mobi support

* Wed Jan 01 2014 Rex Dieter <rdieter@fedoraproject.org> 4.12.0-3
- KDE4_BUILD_TESTS:BOOL=ON

* Mon Dec 30 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.12.0-2
- rebuild against fixed qmobipocket
- BR: libkexiv2-devel

* Thu Dec 19 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.12.0-1
- 4.12.0

* Sun Dec 01 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.97-1
- 4.11.97

* Thu Nov 21 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.95-1
- 4.11.95

* Sat Nov 16 2013 Rex Dieter <rdieter@fedoraproject.org> 4.11.90-2
- +mobipocket support

* Sat Nov 16 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.90-1
- 4.11.90

* Sat Nov 02 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.3-1
- 4.11.3

* Sat Sep 28 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.2-1
- 4.11.2

* Tue Sep 03 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.11.1-1
- 4.11.1

* Thu Aug 08 2013 Than Ngo <than@redhat.com> - 4.11.0-1
- 4.11.0

* Thu Jul 25 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.97-1
- 4.10.97

* Tue Jul 23 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.95-1
- 4.10.95

* Thu Jun 27 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.90-1
- 4.10.90

* Sat Jun 01 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.4-1
- 4.10.4

* Mon May 06 2013 Than Ngo <than@redhat.com> - 4.10.3-1
- 4.10.3

* Tue Apr 09 2013 Than Ngo <than@redhat.com> - 4.10.2-3
- don't build component if ACTIVEAPP_FOUND FALSE

* Fri Apr 05 2013 Than Ngo <than@redhat.com> - 4.10.2-2
- BR on plasma-mobile-devel only in fedora

* Sun Mar 31 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.2-1
- 4.10.2

* Sun Mar 03 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.10.1-1
- 4.10.1

* Thu Jan 31 2013 Than Ngo <than@redhat.com> - 4.10.0-1
- 4.10.0
- get rid of gcc overflow warning

* Tue Jan 22 2013 Rex Dieter <rdieter@fedoraproject.org> 4.9.98-2
- filename encoding fix (#747976, kde#313700)

* Sun Jan 20 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.9.98-1
- 4.9.98

* Fri Jan 04 2013 Rex Dieter <rdieter@fedoraproject.org> - 4.9.97-1
- 4.9.97

* Thu Dec 20 2012 Rex Dieter <rdieter@fedoraproject.org> - 4.9.95-1
- 4.9.95

* Tue Dec 04 2012 Rex Dieter <rdieter@fedoraproject.org> - 4.9.90-1
- 4.9.90

* Mon Dec 03 2012 Than Ngo <than@redhat.com> - 4.9.4-1
- 4.9.4

* Sat Nov 03 2012 Rex Dieter <rdieter@fedoraproject.org> - 4.9.3-1
- 4.9.3

* Wed Oct 24 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.2-2
- rebuild (libjpeg-turbo v8)

* Fri Sep 28 2012 Rex Dieter <rdieter@fedoraproject.org> - 4.9.2-1
- 4.9.2

* Mon Sep 03 2012 Than Ngo <than@redhat.com> -  7 :4.9.1-1
- 4.9.1

* Sat Aug 18 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.0-3
- followup fix for KXMLGUIClient (hang on close)

* Sun Aug 12 2012 Rex Dieter <rdieter@fedoraproject.org> 4.9.0-2
- KXMLGUIClient memory corruption warning (kde#261538)

* Thu Jul 26 2012 Lukas Tinkl <ltinkl@redhat.com> - 4.9.0-1
- 4.9.0

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.8.97-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Wed Jul 11 2012 Rex Dieter <rdieter@fedoraproject.org> - 4.8.97-1
- 4.8.97

* Wed Jun 27 2012 Rex Dieter <rdieter@fedoraproject.org> - 4.8.95-1
- 4.8.95

* Sat Jun 09 2012 Rex Dieter <rdieter@fedoraproject.org> - 4.8.90-1
- 4.8.90

* Sat May 26 2012 Jaroslav Reznik <jreznik@redhat.com> - 4.8.80-1
- 4.8.80

* Wed May 16 2012 Marek Kasik <mkasik@redhat.com> - 4.8.3-4
- Rebuild (poppler-0.20.0)

* Wed May 09 2012 Than Ngo <than@redhat.com> - 4.8.3-3
- add fedora/rhel condition

* Tue May 08 2012 Rex Dieter <rdieter@fedoraproject.org> 4.8.3-2
- rebuild (libtiff)

* Mon Apr 30 2012 Jaroslav Reznik <jreznik@redhat.com> - 4.8.3-1
- 4.8.3

* Fri Mar 30 2012 Rex Dieter <rdieter@fedoraproject.org> - 4.8.2-1
- 4.8.2

* Wed Mar 07 2012 Rex Dieter <rdieter@fedoraproject.org> 4.8.1-2
- s/kdebase-runtime/kde-runtime/

* Mon Mar 05 2012 Jaroslav Reznik <jreznik@redhat.com> - 4.8.1-1
- 4.8.1

* Sun Jan 22 2012 Rex Dieter <rdieter@fedoraproject.org> - 4.8.0-1
- 4.8.0

* Wed Jan 04 2012 Radek Novacek <rnovacek@redhat.com> - 4.7.97-1
- 4.7.97

* Wed Dec 21 2011 Radek Novacek <rnovacek@redhat.com> - 4.7.95-1
- 4.7.95

* Sun Dec 04 2011 Rex Dieter <rdieter@fedoraproject.org> - 4.7.90-1
- 4.7.90

* Fri Nov 25 2011 Jaroslav Reznik <jreznik@redhat.com> 4.7.80-1
- 4.7.80 (beta 1)

* Tue Nov 15 2011 Rex Dieter <rdieter@fedoraproject.org> 4.7.3-2
- okular-part subpkg
- BR: libjpeg-devel

* Sat Oct 29 2011 Rex Dieter <rdieter@fedoraproject.org> 4.7.3-1
- 4.7.3
- more pkgconfig-style deps

* Wed Oct 05 2011 Rex Dieter <rdieter@fedoraproject.org> 4.7.2-1
- 4.7.2

* Tue Sep 06 2011 Than Ngo <than@redhat.com> - 4.7.1-1
- 4.7.1

* Tue Jul 26 2011 Jaroslav Reznik <jreznik@redhat.com> 4.7.0-1
- 4.7.0

* Mon Jul 18 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.95-4
- %%postun: +update-desktop-database

* Mon Jul 18 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.95-3
- BR: pkgconfig(qca2)

* Fri Jul 15 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.95-2
- bump release

* Mon Jul 11 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.95-1
- 4.6.95
- fix URL

* Wed Jul 06 2011 Rex Dieter <rdieter@fedoraproject.org> 4.6.90-3
- fix Source URL
- Conflicts: kdegraphics < 7:4.6.90-10

* Tue Jul 05 2011 Rex Dieter <rdieter@fedoraproject.org>  4.6.90-2
- first try
