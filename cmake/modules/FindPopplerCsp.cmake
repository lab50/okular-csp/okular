set(PopplerCsp_known_components
    Core
    Qt5
)

# Core component.
set(PopplerCsp_Core_pkg_config "poppler-csp")
set(PopplerCsp_Core_component_deps "")
set(PopplerCsp_Core_header_subdir "poppler-csp")
set(PopplerCsp_Core_header "poppler-config.h" "qt5/poppler-qt5.h")
set(PopplerCsp_Core_lib "poppler-csp")

# Qt5 component.
set(PopplerCsp_Qt5_pkg_config "poppler-csp-qt5")
set(PopplerCsp_Qt5_component_deps "Core")
set(PopplerCsp_Qt5_header_subdir "poppler-csp/qt5")
set(PopplerCsp_Qt5_header "poppler-qt5.h")
set(PopplerCsp_Qt5_lib "poppler-csp-qt5")

ecm_find_package_parse_components(PopplerCsp
    RESULT_VAR PopplerCsp_components
    KNOWN_COMPONENTS ${PopplerCsp_known_components}
)
ecm_find_package_handle_library_components(PopplerCsp
    COMPONENTS ${PopplerCsp_components}
)

find_package_handle_standard_args(PopplerCsp
    FOUND_VAR
        PopplerCsp_FOUND
    REQUIRED_VARS
        PopplerCsp_LIBRARIES
    VERSION_VAR
        PopplerCsp_VERSION
    HANDLE_COMPONENTS
)
