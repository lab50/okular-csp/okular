#ifndef LICENSESETTINGSWIDGET_H
#define LICENSESETTINGSWIDGET_H

#include <QWidget>

#include <poppler-form.h>

class QFormLayout;
class QPushButton;
class QHBoxLayout;

class LicenseSettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LicenseSettingsWidget(QWidget *parent, Poppler::LicenseInfo *license, bool licenseFileExists, const QString &errorMsg);

private:
    static const QString LICENSES_PATTERN;
    static const QString SUPPORT_EMAIL;
    static const int STATIC_ROWS_NUM;

    QFormLayout *m_layout;
    QPushButton *m_installButton;
    QPushButton *m_supportButton;

    static QHBoxLayout *addIdRow(const QString &value);
    void updateForm(Poppler::LicenseInfo *license, bool licenseFileExists, const QString &errorMsg);

private Q_SLOTS:
    void onInstallButtonClick();
    void onSupportButtonClick();
};

// BrokenLicenseSettingsWidget displays an error message.
class BrokenLicenseSettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit BrokenLicenseSettingsWidget(QWidget *parent, const QString &errorMsg);

private:
    QFormLayout *m_layout;
};

#endif
