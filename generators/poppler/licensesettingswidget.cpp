#include "licensesettingswidget.h"

#include <KLocalizedString>
#include <QApplication>
#include <QLabel>
#include <QDesktopServices>
#include <KMessageBox>
#include <QFileDialog>
#include <QFormLayout>
#include <QPushButton>
#include <QHBoxLayout>
#include <QClipboard>
#include <QDebug>

#include "core/version.h"
#include <os.h>

static QString getEnvironment()
{
    QString osInfo = QString::fromStdString(getOsInfo());
    QString cspInfo = Poppler::getCspInfo();
    return osInfo + ", " + cspInfo;
}

const QString LicenseSettingsWidget::LICENSES_PATTERN = i18nc("licenseform", "License Files") + " (*.lic *.key *.asc)";
const QString LicenseSettingsWidget::SUPPORT_EMAIL = "support@okulargost.ru";
const int LicenseSettingsWidget::STATIC_ROWS_NUM = 7;

LicenseSettingsWidget::LicenseSettingsWidget(QWidget *parent, Poppler::LicenseInfo *license, bool licenseFileExists, const QString &errorMsg)
    : QWidget(parent),
      m_layout(new QFormLayout(this)),
      m_installButton(new QPushButton(QIcon::fromTheme(QStringLiteral("dialog-ok")), i18nc("licenseform", "Install license..."))),
      m_supportButton(new QPushButton(QIcon::fromTheme(QStringLiteral("help-contents")), i18nc("licenseform", "Technical support")))
{
    m_layout->addRow(i18nc("licenseform", "Machine id:"), addIdRow(QString::fromStdString(getMachineId())));
    QLabel *environmentLabel = new QLabel(::getEnvironment());
    m_layout->addRow(i18nc("licenseform", "Environment:"), environmentLabel);
    QLabel *versionLabel = new QLabel(QStringLiteral(OKULAR_VERSION_STRING));
    m_layout->addRow(i18nc("licenseform", "Okular GOST version:"), versionLabel);

    m_layout->addRow(new QLabel(this));
    connect(m_installButton, SIGNAL(clicked()), this, SLOT(onInstallButtonClick()));
    m_layout->addRow("", m_installButton);
    m_layout->addRow(new QLabel(this));
    connect(m_supportButton, SIGNAL(clicked()), this, SLOT(onSupportButtonClick()));
    m_layout->addRow("", m_supportButton);

    updateForm(license, licenseFileExists, errorMsg);
}

QHBoxLayout *LicenseSettingsWidget::addIdRow(const QString &value)
{
    QLabel *idLabel = new QLabel(value);
    auto copyButton = new QPushButton(QIcon::fromTheme(QStringLiteral("edit-copy")), "");
    copyButton->setToolTip(i18nc("licenseform", "Copy"));
    copyButton->setStyleSheet(QString("QPushButton { border: none; }"));
    QHBoxLayout *rowLayout = new QHBoxLayout();
    rowLayout->addWidget(idLabel);
    rowLayout->addWidget(copyButton);
    connect(
        copyButton,
        &QPushButton::clicked,
        [value]() {
            QClipboard *clip = QApplication::clipboard();
            clip->setText(value);
        }
    );
    return rowLayout;
}

void LicenseSettingsWidget::updateForm(Poppler::LicenseInfo *license, bool licenseFileExists, const QString &errorMsg)
{
    int rowsNumber = m_layout->rowCount();
    for (int i = 0;  i < rowsNumber - STATIC_ROWS_NUM; i++) {
        m_layout->removeRow(0);
    }

    if (license == nullptr) {
        if (licenseFileExists) {
            QString msg = i18nc("licenseform", "Invalid license");
            if (!errorMsg.isEmpty()) {
                msg += ": " + errorMsg;
            }
            m_layout->insertRow(0, new QLabel(msg));
        } else {
            m_layout->insertRow(0, i18nc("licenseform", "Product:"), new QLabel(i18nc("licenseform", "free version")));
        }
        m_installButton->setVisible(true);
        m_supportButton->setVisible(false);
    } else {
        m_supportButton->setVisible(true);
        m_layout->insertRow(0, i18nc("licenseform", "License id:"), addIdRow(license->id()));
        m_layout->insertRow(0, i18nc("licenseform", "E-mail:"), new QLabel(license->email()));
        m_layout->insertRow(
            0,
            i18nc("licenseform", "Number:"),
            new QLabel(QString::number(license->number()))
        );
        m_layout->insertRow(0, i18nc("licenseform", "Product:"), new QLabel(license->product()));
        m_layout->insertRow(0, i18nc("licenseform", "Term:"), new QLabel(license->term()));
        m_layout->insertRow(0, i18nc("licenseform", "License owner:"), new QLabel(license->name()));
        m_installButton->setVisible(!license->isValid());
    }
}

void LicenseSettingsWidget::onInstallButtonClick()
{
    QString licensePath = QFileDialog::getOpenFileName(this, i18nc("licenseform", "Select License File"), QDir::currentPath(), LICENSES_PATTERN);
    if (licensePath.isEmpty()) {
        // Cancel = do nothing.
        return;
    }

    QString errorMsg;
    if (Poppler::installLicense(licensePath, errorMsg).isEmpty()) {
        QString msg = i18nc("licenseform", "License installation failed.");
        if (!errorMsg.isEmpty()) {
            msg += "\n";
            msg += i18n("Additional info: %1.", errorMsg);
        }
        KMessageBox::error(this, msg);
    }

    bool licenseFileExists;
    auto license = Poppler::loadLicense(licenseFileExists, errorMsg);
    updateForm(license.get(), licenseFileExists, errorMsg);
}

void LicenseSettingsWidget::onSupportButtonClick()
{
    QDesktopServices::openUrl(QUrl("mailto:" + SUPPORT_EMAIL));
}

BrokenLicenseSettingsWidget::BrokenLicenseSettingsWidget(QWidget *parent, const QString &errorMsg)
    : QWidget(parent),
      m_layout(new QFormLayout(this))
{
    m_layout->addRow(new QLabel(i18nc("licenseform", "Signature plugin not loaded.")));
    if (!errorMsg.isEmpty()) {
        m_layout->addRow(new QLabel(i18n("Additional info: %1.", errorMsg)));
    }
}
