/*
    SPDX-FileCopyrightText: 2007 Pino Toscano <pino@kde.org>
    SPDX-FileCopyrightText: 2022 Georgiy Sgibnev <georgiy@lab50.net>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef _ABOUTDATA_H_
#define _ABOUTDATA_H_

#include <KAboutData>

#include "core/version.h"
#include <eula.h>

#include <KLocalizedString>

inline KAboutData okularAboutData()
{
    KAboutData about(QStringLiteral("okularcsp"),
                     i18n("Okular GOST"),
                     QStringLiteral(OKULAR_VERSION_STRING),
                     i18n("Okular GOST, a PDF viewer and a signature creator"),
                     KAboutLicense::Custom,
                     i18n("(C) 2021 Laboratory 50\n"
                          "(C) 2002 Wilco Greven, Christophe Devriese\n"
                          "(C) 2004-2005 Enrico Ros\n"
                          "(C) 2005 Piotr Szymanski\n"
                          "(C) 2004-2017 Albert Astals Cid\n"
                          "(C) 2006-2009 Pino Toscano"),
                     QString(),
                     QStringLiteral("https://lab50.net"),
                     QStringLiteral("support@okulargost.ru"));

    about.setLicenseTextFile(QString::fromUtf8(getEulaPath().c_str()));
    about.addAuthor(QStringLiteral("Georgiy Sgibnev"), i18n("Developer"), QStringLiteral("georgiy@lab50.net"));
    about.addAuthor(QStringLiteral("Pino Toscano"), i18n("Former maintainer"), QStringLiteral("pino@kde.org"));
    about.addAuthor(QStringLiteral("Tobias Koenig"), i18n("Lots of framework work, ODT and FictionBook backends"), QStringLiteral("tokoe@kde.org"));
    about.addAuthor(QStringLiteral("Albert Astals Cid"), i18n("Developer"), QStringLiteral("aacid@kde.org"));
    about.addAuthor(QStringLiteral("Piotr Szymanski"), i18n("Created Okular from KPDF codebase"), QStringLiteral("djurban@pld-dc.org"));
    about.addAuthor(QStringLiteral("Enrico Ros"), i18n("KPDF developer"), QStringLiteral("eros.kde@email.it"));
    about.addCredit(QStringLiteral("Eugene Trounev"), i18n("Annotations artwork"), QStringLiteral("eugene.trounev@gmail.com"));
    about.addCredit(QStringLiteral("Jiri Baum - NICTA"), i18n("Table selection tool"), QStringLiteral("jiri@baum.com.au"));
    about.addCredit(QStringLiteral("Fabio D'Urso"), i18n("Annotation improvements"), QStringLiteral("fabiodurso@hotmail.it"));

    return about;
}

#endif
