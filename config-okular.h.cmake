/* Defines if force the use DRM in okular */
#define OKULAR_FORCE_DRM ${_OKULAR_FORCE_DRM}

/* Defines if the purpose framework is available */
#define PURPOSE_FOUND ${PURPOSE_FOUND}

/* Defines if the kf5 crash is available */
#cmakedefine WITH_CRASH 1

/* Defines whether the malloc_trim method from malloc.h is available */
#cmakedefine01 HAVE_MALLOC_TRIM

/* Defines whether we are building with dbus enabled */
#cmakedefine01 HAVE_DBUS
