/*
    SPDX-FileCopyrightText: 2006 Luigi Toscano <luigi.toscano@tiscali.it>
    SPDX-FileCopyrightText: 2008 Pino Toscano <pino@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "utils.h"
#include "utils_p.h"

#include "debug_p.h"
#include "settings_core.h"

#include <QApplication>
#include <QFileInfo>
#include <QIODevice>
#include <QImage>
#include <QRect>
#include <QScreen>
#include <QWidget>
#include <QWindow>

#include <fontconfig/fontconfig.h>

using namespace Okular;

QRect Utils::rotateRect(const QRect &source, int width, int height, int orientation) // clazy:exclude=function-args-by-value TODO remove the & when we do a BIC change elsewhere
{
    QRect ret;

    // adapt the coordinates of the boxes to the rotation
    switch (orientation) {
    case 1:
        ret = QRect(width - source.y() - source.height(), source.x(), source.height(), source.width());
        break;
    case 2:
        ret = QRect(width - source.x() - source.width(), height - source.y() - source.height(), source.width(), source.height());
        break;
    case 3:
        ret = QRect(source.y(), height - source.x() - source.width(), source.height(), source.width());
        break;
    case 0:  // no modifications
    default: // other cases
        ret = source;
    }

    return ret;
}

QSizeF Utils::realDpi(QWidget *widgetOnScreen)
{
    const QScreen *screen = widgetOnScreen && widgetOnScreen->window() && widgetOnScreen->window()->windowHandle() ? widgetOnScreen->window()->windowHandle()->screen() : qGuiApp->primaryScreen();

    if (screen) {
        const QSizeF res(screen->physicalDotsPerInchX(), screen->physicalDotsPerInchY());
        if (res.width() > 0 && res.height() > 0) {
            if (qAbs(res.width() - res.height()) / qMin(res.height(), res.width()) < 0.05) {
                return res;
            } else {
                qCDebug(OkularCoreDebug) << "QScreen calculation returned a non square dpi." << res << ". Falling back";
            }
        }
    }
    return QSizeF(72, 72);
}

inline static bool isPaperColor(QRgb argb, QRgb paperColor)
{
    return (argb & 0xFFFFFF) == (paperColor & 0xFFFFFF); // ignore alpha
}

NormalizedRect Utils::imageBoundingBox(const QImage *image)
{
    if (!image)
        return NormalizedRect();

    const int width = image->width();
    const int height = image->height();
    const QRgb paperColor = SettingsCore::paperColor().rgb();
    int left, top, bottom, right, x, y;

#ifdef BBOX_DEBUG
    QTime time;
    time.start();
#endif

    // Scan pixels for top non-white
    for (top = 0; top < height; ++top)
        for (x = 0; x < width; ++x)
            if (!isPaperColor(image->pixel(x, top), paperColor))
                goto got_top;
    return NormalizedRect(0, 0, 0, 0); // the image is blank
got_top:
    left = right = x;

    // Scan pixels for bottom non-white
    for (bottom = height - 1; bottom >= top; --bottom)
        for (x = width - 1; x >= 0; --x)
            if (!isPaperColor(image->pixel(x, bottom), paperColor))
                goto got_bottom;
    Q_ASSERT(0); // image changed?!
got_bottom:
    if (x < left)
        left = x;
    if (x > right)
        right = x;

    // Scan for leftmost and rightmost (we already found some bounds on these):
    for (y = top; y <= bottom && (left > 0 || right < width - 1); ++y) {
        for (x = 0; x < left; ++x)
            if (!isPaperColor(image->pixel(x, y), paperColor))
                left = x;
        for (x = width - 1; x > right + 1; --x)
            if (!isPaperColor(image->pixel(x, y), paperColor))
                right = x;
    }

    NormalizedRect bbox(QRect(left, top, (right - left + 1), (bottom - top + 1)), image->width(), image->height());

#ifdef BBOX_DEBUG
    qCDebug(OkularCoreDebug) << "Computed bounding box" << bbox << "in" << time.elapsed() << "ms";
#endif

    return bbox;
}

void Okular::copyQIODevice(QIODevice *from, QIODevice *to)
{
    QByteArray buffer(65536, '\0');
    qint64 read = 0;
    qint64 written = 0;
    while ((read = from->read(buffer.data(), buffer.size())) > 0) {
        written = to->write(buffer.constData(), read);
        if (read != written)
            break;
    }
}

QTransform Okular::buildRotationMatrix(Rotation rotation)
{
    QTransform matrix;
    matrix.rotate((int)rotation * 90);

    switch (rotation) {
    case Rotation90:
        matrix.translate(0, -1);
        break;
    case Rotation180:
        matrix.translate(-1, -1);
        break;
    case Rotation270:
        matrix.translate(-1, 0);
        break;
    default:;
    }

    return matrix;
}

QString Utils::selectSansSerifFont()
{
#if defined(WIN32)
    QString exeDirectoryPath = QCoreApplication::applicationDirPath();
    QString fontPath = exeDirectoryPath + "/../pt-sans-regular.ttf";
#else
    QString fontPath = QStandardPaths::locate(
        QStandardPaths::GenericDataLocation,
        QStringLiteral("okular-csp-common/pt-sans-regular.ttf")
    );
#endif
    if (!fontPath.isEmpty() && QFileInfo::exists(fontPath)) {
        return fontPath;
    }

    QString locale = QLocale().name(); // Returns something like en_US.
    locale.replace('_', '-');

    FcConfig *config = FcInitLoadConfigAndFonts();
    FcLangSet *langSet = FcLangSetCreate();
    FcLangSetAdd(langSet, (const FcChar8 *)locale.toStdString().c_str());
    FcPattern *pattern = FcNameParse((const FcChar8 *)"Sans-serif");
    FcPatternAddLangSet(pattern, FC_LANG, langSet);
    FcConfigSubstitute(config, pattern, FcMatchPattern);
    FcDefaultSubstitute(pattern);

    fontPath = "";
    FcResult result;
    FcPattern *font = FcFontMatch(config, pattern, &result);
    if (font) {
        FcChar8 *buffer = nullptr;
        if (FcPatternGetString(font, FC_FILE, 0, &buffer) == FcResultMatch) {
            fontPath = QString::fromUtf8((char *)buffer);
        }
        FcPatternDestroy(font);
    }
    FcPatternDestroy(pattern);
    FcLangSetDestroy(langSet);
    FcConfigDestroy(config);
    return fontPath;
}

/* kate: replace-tabs on; indent-width 4; */
